FROM apiaryio/emcc

WORKDIR /opt
ADD hdf5-1.10.5.tar.bz2 /opt/hdf5-1.10.5.tar.bz2
RUN tar xjf hdf5-1.10.5.tar.bz2
RUN cd /opt/hdf5-1.10.5 && emconfigure